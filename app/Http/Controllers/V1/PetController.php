<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Services\PetService;

class PetController extends Controller
{
    public function __construct(PetService $petService)
    {
        $this->petService = $petService;
    }

    /**
     * Search for pets within a given proximity
     */
    public function search()
    {
        $params = request()->all();
        $response = $this->petService->search($params);
        return response()->json([
            'message' => $response['message'],
            'body' => $response['body']
        ], $response['code']);
    }
}
