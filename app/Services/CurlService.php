<?php

namespace App\Services;

class CurlService
{
    public function request(
        $method = 'GET',
        string $url,
        array $body = [],
        array $headers = []
    ) {
        // prepare data
        $method = strtoupper($method);
        if ($method == 'POST') {
            $params = $body;
        }
        if ($method == 'GET' && isset($body)) {
            $params = null;
            $url .= '?';
            $count = count($body);
            $index = 1;
            foreach ($body as $key => $value) {
                $url .= $key . "=" . $value;
                if ($count > $index) {
                    $url .= '&';
                }
                $index ++;
            }
        }

        $curl = curl_init();
        curl_setopt_array(
            $curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POSTFIELDS => $params,
                CURLOPT_HTTPHEADER => $headers
            )
        );

        // disable SSL verification in local environments only (this is necessary for Mac users with Valet environments)
        if (config('site.app_env') == 'local') {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        }

        // process curl request
        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return [
                'error' => true,
                'message' => "cURL Error #:" . $err
            ];
        } else {
            return json_decode($response);
        }
    }

    /**
    * Get
    */
    public function get(string $url, array $body = [], array $headers = []) {
        return $this->request('GET', $url, $body, $headers);
    }

    /**
    * Post
    */
    public function post(string $url, array $body = [], array $headers = []) {
        return $this->request('POST', $url, $body, $headers);
    }
}
