<?php

namespace App\Services;

use App\Services\CurlService;

class petService
{
    public function __construct(CurlService $curlService)
    {
        $this->curlService = $curlService;
        $this->apiUrl = 'http://api.adoptapet.com/search/ra1';
        $this->key = 'ebe25d5f3e2cba56bbca97e7e7ef1250';
    }

    /**
     * Assign common parameters
     */
    public function assignCommonParams(array $params)
    {
        $params['key'] = $this->key;
        $params['output'] = 'json';
        $params['start_number'] = $params['start_number'] ?? 1;
        $params['end_number'] = $params['end_number'] ?? 10;
        return $params;
    }

    /**
     * Search for pets within a given proximity
     */
    public function search(array $params = [])
    {
        $params = $this->assignCommonParams($params);
        $response = $this->curlService->get("$this->apiUrl/pet_search", $params);
        if (isset($response->pets)) {

            // assign numeric values to ages so that results can be sorted
            foreach ($response->pets as $index => $pet) {
                if ($pet->age == 'senior') {
                    $response->pets[$index]->ageIndex = 4;
                }
                if ($pet->age == 'adult') {
                    $response->pets[$index]->ageIndex = 3;
                }
                if ($pet->age == 'young') {
                    $response->pets[$index]->ageIndex = 2;
                }
                if ($pet->age == 'puppy' || $pet->age == 'kitten') {
                    $response->pets[$index]->ageIndex = 1;
                }
            }

            return [
                'message' => $response->returned_pets . ' pets found',
                'body' => $response,
                'code' => 200
            ];
        }
        return [
            'message' => 'No pets found',
            'body' => null,
            'code' => 400
        ];
    }
}
